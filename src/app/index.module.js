(function() {
  'use strict';

  angular
    .module('chatlanin', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ui.router', 'mgcrea.ngStrap', 'toastr', 'LocalStorageModule', 'satellizer',
      /*'angular-loading-bar', */'pascalprecht.translate', 'gc.bundle', 'faq'
    ]);

})();
