(function() {
  'use strict';

  angular
    .module('chatlanin')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($scope, $appConfig, AuthService, $rootScope, $timeout) {
    var vm = this;

    $scope.appReady = true;

    $scope.isAuthenticated = AuthService.isAuthenticated;
  }
})();
