(function() {
  'use strict';

  angular
    .module('chatlanin')
    .config(config);

  /** @ngInject */
  function config($logProvider, toastrConfig, $sceDelegateProvider, $locationProvider, $translateProvider) {
    $locationProvider.html5Mode(true);
    $sceDelegateProvider.resourceUrlWhitelist([
      'self'
    ]);
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;

    $translateProvider.useSanitizeValueStrategy(null);

    //gcBreadcrumbsProvider.setShowSingleBreadcrumb(true);
  }

})();
