/* global malarkey:false, moment:false */
(function($window) {
  'use strict';

  angular
    .module('chatlanin')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .constant('_', window._);

})();
