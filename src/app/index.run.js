(function() {
  'use strict';

  angular
    .module('chatlanin')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log, $rootScope, $urlRouter, AuthService, $state, $timeout) {

    $log.debug('runBlock end');

    $rootScope.$on('$stateChangeStart', function(e, toState, toParams, fromState, fromParams){
      if (_.has(toState, 'data') && _.isObject(toState.data) && toState.data.loginRequired && !AuthService.isAuthenticated()) {
        e.preventDefault();
        AuthService.afterLoginState = {name: toState.name, params: toParams};
        $state.transitionTo('auth.login');
      }
    });

    $timeout(function(){
      var el = angular.element('.page-loading-frame');
      el.fadeOut(500);
    });
  }

})();
