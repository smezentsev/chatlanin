(function () {
  'use strict';

  angular.module('gc.model')
    .factory('modelFactory', modelFactory);

  function modelFactory($resource) {
    return function BaseModel(url, meta, model) {
      var self = this;
      this.model = model;

      model.isNewRecord = true;

      if (_.isArray(meta)) {
        _.each(meta, function (attr) {
          if (model.attributes[attr] === undefined) {
            model.attributes[attr] = null;
          } else {
            model.isNewRecord = false;
            if (_.has(model, 'classMap') && _.isFunction(model.classMap[attr])) {
              if (_.isArray(model.attributes[attr])) {
                _.each(model.attributes[attr], function(classModel, key){
                  model.attributes[attr][key] = new model.classMap[attr](model.attributes[attr][key]);
                });
              } else {
                model.attributes[attr] = new model.classMap[attr](model.attributes[attr]);
              }
            }
          }
        })
      }

      this.provider = $resource(url, {}, {
        create: {method: 'POST'},
        update: {method: 'PUT'},
        delete: {method: 'DELETE'},
        query: {method: 'GET', isArray: true},
        get: {method: 'GET'}
      });

      this.save = function () {
        if (self.model.isNewRecord)
          return self.provider.create(self.model.attributes);
        else
          return self.provider.update(self.model.attributes);
      };

      this.query = function (criteria) {
        criteria = criteria || {};
        return self.provider.query(criteria, function (response) {
          response.data = [];
          _.each(response, function (item) {
            response.data.push(new model.constructor(item.toJSON()));
          });
          return response;
        });
      };

      this.get = function (criteria) {
        criteria = criteria || {};
        return self.provider.get(criteria, function (response) {
          response.data = new model.constructor(response.toJSON());
          return response;
        });
      };

    };
  }

})();
