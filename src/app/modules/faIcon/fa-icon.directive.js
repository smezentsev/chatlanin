(function(){
  'use strict';

  angular.module('gc.faIcon', [])
    .directive('faIcon', faIcon);


  function faIcon() {
    var directive = {
      restrict: 'EA',
      transclude: true,
      replace: true,
      scope: {
        'faIcon': '=',
        'icon': '=icon'
      },
      template: '<span><span class="fa fa-{{ic}}"></span><ng-transclude></ng-transclude></span>',
      link: function(scope) {
        var initIcon = function() {
          scope.ic = scope.faIcon || scope.icon;
        };
        scope.$watch('faIcon', initIcon);
        scope.$watch('icon', initIcon);
      }
    };

    return directive;
  }

})();
