(function(){
  'use strict';

  angular.module('gc.appConfig')
    .constant('localStorage', window.localStorage)
    .service('$appConfig', $appConfig)
    .provider('$appConfig', $appConfigProvider);

  function $appConfig(config) {
    this.config = config;
  }

  function $appConfigProvider($translateProvider) {
    var self = this;

    $translateProvider.useLocalStorage();

    $translateProvider.useStaticFilesLoader({
      files: [
        {
          prefix: 'app/translations/',
          suffix: '/translate.json'
        }
      ]
    });

    var defaultLanguage = 'en-US';

    $translateProvider.preferredLanguage(defaultLanguage);

    this.config = {
      apiUrl: 'http://api.chatlanin/api',
      languages: [
        {
          key: 'en-US',
          name: 'English'
        },
        {
          key: 'ru-RU',
          name: 'Русский'
        }
      ]
    };

    this.$get = [function() {
      return new $appConfig(self.config);
    }];
  }

})();
