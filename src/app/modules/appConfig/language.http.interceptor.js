(function(){
  'use strict';

  angular.module('gc.appConfig')
    .constant('localStorage', window.localStorage)
    .factory('LanguageHttpInterceptor', LanguageHttpInterceptor)
    .config(['$httpProvider', function($httpProvider) {
      $httpProvider.interceptors.push('LanguageHttpInterceptor');
    }]);

  function LanguageHttpInterceptor($translate) {
    return {
      request: function(request) {
        if (request.url.match(/\/api\/(.*)/)) {
          request.headers.Language = $translate.use();
        }
        return request;
      }
    };
  }

})();
