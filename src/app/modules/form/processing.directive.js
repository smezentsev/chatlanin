(function(){
  'use strict';

  angular.module('gc.form')
    .directive('gcSpinner', spinner);

  function spinner() {
    var directive = {
      restrict: 'A',
      transclude: true,
      template: '<div ng-hide="!showSpinner" class="panel-refresh-layer"><img src="assets/img/loaders/default.gif"/></div><ng-transclude></ng-transclude>',
      link: function(scope, element, attrs) {
        scope.showSpinner = false;
        element.addClass('panel-body');

        scope.$watch(function(){ return scope.$eval(attrs.gcSpinner) }, function(q) {
          var spinnerElement = element.parent().find('.panel-refresh-layer');
          scope.showSpinner = q;
          spinnerElement.width(element.outerWidth()).height(element.outerHeight());
        });
      }
    };

    return directive;
  }

})();
