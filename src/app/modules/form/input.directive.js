(function () {
  'use strict';

  angular.module('gc.form')
    .directive('gcFormInput', formInput);

  function formInput($timeout, $translate, $filter, $rootScope) {

    var performTranslation = function (translateId, input, validator, translateFieldLabel) {
      var translateOptions = {
        translateId: translateId,
        options: {fieldname: $filter('translate')(translateFieldLabel)}
      };
      switch (validator) {
        case('minlength'):
          translateOptions.options.count = input.attr(validator);
          break;
        case('maxlength'):
          translateOptions.options.count = input.attr(validator);
          break;
        default:
          break;
      }
      return translateOptions;
    };

    var directive = {
      restrict: 'E',
      transclude: true,
      replace: true,
      scope: {
        size: '@size',
        inputWrapperColClass: '@wrapperColClass',
        icon: '@icon',
        translateFieldLabel: '@translateFieldLabel',
        errorLabelTemplateUrl: '@errorLabelTemplateUrl'
      },
      require: '^form',
      templateUrl: function (element, attrs) {
        return ((attrs && attrs.templateUrl) ? attrs.templateUrl : 'app/modules/form/input.html')
      },
      link: function (scope, element, attrs, ctrl, $q) {
        scope.errorLabelTemplateUrl = scope.errorLabelTemplateUrl || 'app/modules/form/label_error.html';
        scope.inputWrapperColClass = scope.inputWrapperColClass || null;
        $timeout(function () {
          var input = element.find('.form-control');
          scope.fieldName = input.attr('name');
          scope.fieldId = input.attr('id');
          scope.$form = ctrl;
          scope.$modelCtrl = ctrl[scope.fieldName];

          var prepareMessages = function () {
            scope.validators = {};
            _.each(scope.$modelCtrl.$validators, function (value, key) {
              if (key != 'serverValidator') {
                var translateId = 'models.' + scope.$form.$name + '.validation.' + scope.fieldName + '.' + key;
                $translate(translateId)
                  .then(function () {
                    scope.validators[key] = performTranslation(translateId, input, key, scope.translateFieldLabel);
                  }, function () {
                    translateId = 'models.' + scope.$form.$name + '.validation.' + key;
                    $translate(translateId)
                      .then(function () {
                        scope.validators[key] = performTranslation(translateId, input, key, scope.translateFieldLabel);
                      }, function () {
                        translateId = 'models.common.validation.' + key;
                        scope.validators[key] = performTranslation(translateId, input, key, scope.translateFieldLabel);
                      });
                  });
              }
            });
          };

          prepareMessages();

          $rootScope.$on('$translateChangeSuccess', function (e) {
            prepareMessages();
          });

        });
      },
      controller: function ($scope) {
        var vm = this;
      },
      controllerAs: 'fieldCtrl'
    };

    return directive;
  }


})();
