(function(){
  'use strict';

  angular.module('gc.pageTitle', ['gc.faIcon'])
    .directive('pageTitle', pageTitle);

  function pageTitle() {
    var directive = {
      restrict: 'E',
      templateUrl: function(element, attrs) {
        return attrs && attrs.templateUrl ? attrs.templateUrl : 'app/modules/pageTitle/main.html'
      },
      controller: function($scope, $state) {
        $scope.$state = $state.current;
        $scope.$on('$stateChangeSuccess', function(){
          $scope.$state = $state.current;
        });
      }
    };

    return directive;
  }


})();
