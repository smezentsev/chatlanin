(function(){
  'use strict';

  angular.module('gc.breadcrumbs')
    .directive('gcBreadcrumbs', breadcrumbsDirective)
    .service('gcBreadcrumbs', breadcrumbsService);

  function breadcrumbsDirective() {
    var directive = {
      restrict: 'E',
      link: function(){},
      templateUrl: function(element, attrs) {
        return attrs && attrs.templateUrl ? attrs.templateUrl : 'app/modules/breadcrumbs/main.html'
      },
      controller: BreadcrumbsController
    };

    return directive;
  }

  function BreadcrumbsController($scope, gcBreadcrumbs) {

    var rebuildBreadcrumbs = function() {
      $scope.states = gcBreadcrumbs.getTree();
    };

    rebuildBreadcrumbs();
    $scope.$on('$stateChangeSuccess', rebuildBreadcrumbs)
  }

  function breadcrumbsService($state) {
    var getTree = function() {
      var states = [];

      _.each($state.$current.path, function(state) {
        state = $state.get(state);
        if (!state.abstract)
          states.push(state);
      });

      if (!getShowSingle() && states.length === 1)
        return [];

      return states;
    };

    var getCurrent = function() {
      return $state.current;
    };

    var getShowSingle = function() {
      return !(_.has($state.current, 'breadcrumbs') && _.has($state.current.breadcrumbs, 'showSingle') && $state.current.breadcrumbs.showSingle === false);
    };

    return {
      getShowSingle: getShowSingle,
      getTree: getTree,
      getCurrent: getCurrent
    }
  }

})();
