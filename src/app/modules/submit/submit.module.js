(function() {
  'use strict';

  angular
    .module('gc.submit', [])
    .constant('_', window._);
})();
