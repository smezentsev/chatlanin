(function () {
  'use strict';

  angular
    .module('gc.submit')
    .directive('gcSubmit', gcSubmit)
    .directive('gcFieldDepend', gcFieldDepend);

  function gcFieldDepend($timeout) {
    return {
      restrict: 'A',
      require: ['ngModel', '^form'],
      link: function(scope, element, attrs, ctrls) {
        var modelCtrl = ctrls[0],
          formCtrl = ctrls[1];

        var addWatcher = function(ctrl) {
          modelCtrl.$viewChangeListeners.push(function(){
            ctrl.$lastCommitValue = '';
            ctrl.$serverErrorMessage = '';
            ctrl.$validate();
          });
        };

        $timeout(function(){
          var dependOn = formCtrl[attrs.gcFieldDepend];

          if (_.isObject(dependOn)) {
            addWatcher(dependOn);
          } else if (_.isArray(dependOn)) {
            _.each(dependOn, addWatcher);
          }
        });
      }
    }
  }

  function gcSubmit($parse, $q) {

    var processError = function(error, ngFormCtrl) {
      var field = error.field,
        message = error.message;
      var modelCtrl = ngFormCtrl[field] || null;
      if (!modelCtrl) return;

      modelCtrl.$lastCommitValue = modelCtrl.$viewValue;
      modelCtrl.$serverErrorMessage = message;

      if (!_.has(modelCtrl.$validators, 'serverValidator')) {
        modelCtrl.$validators.serverValidator = function(modelValue, viewValue) {
          return (viewValue !== modelCtrl.$lastCommitValue)
        };

        modelCtrl.$viewChangeListeners.push(function () {
          if (modelCtrl.$viewValue !== modelCtrl.$lastCommitValue) {
            modelCtrl.$lastCommitValue = '';
            modelCtrl.$serverErrorMessage = '';
          }
        });
      }

      modelCtrl.$validate();
    };

    var processErrors = function(errors, ngFormCtrl) {
      if (_.isArray(errors))
        _.each(errors, function(error) {
          processError(error, ngFormCtrl);
        });
    };

    return {
      restrict: "A",
      require: "^form",
      link: function(scope, element, attrs, ngFormCtrl) {
        attrs.$observe('gcSubmit', function(value){
          if (value) {
            ngFormCtrl.$submitting = false;
            var fn = $parse(value, null, true);
            element.on('submit', function(event){
              var callback = function() {
                return fn(scope, {$event:event});
              };
              ngFormCtrl.$submitting = true;
              var promise = callback();
              if (_.isObject(promise)) {
                promise
                  .then(function(response){
                    ngFormCtrl.$submitting = false;
                    return response;
                  }, function(responseError){
                    ngFormCtrl.$submitting = false;
                    if (_.isObject(responseError) && _.has(responseError, 'status') && responseError.status === 422) {
                      processErrors(responseError.data, ngFormCtrl);
                    }
                    return $q.reject(responseError);
                  })
              } else {
                ngFormCtrl.$submitting = false;
              }
            });
          }
        });
      }
    }
  }

})();
