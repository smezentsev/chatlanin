(function () {
  'use strict';

  angular
    .module('gc.auth')
    .controller('AuthController', AuthController);

  /** @ngInject */
  function AuthController($scope, $state, AuthService) {
    var vm = this;

    if (AuthService.isAuthenticated())
      $state.go('home');
  }

})();
