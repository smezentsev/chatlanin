(function () {
  'use strict';

  angular
    .module('gc.auth')
    .controller('SignupController', SignupController);

  /** @ngInject */
  function SignupController($scope, $state, AuthService, User, $q) {
    var vm = this;

    vm.model = new User();

    vm.submit = function () {
      return vm.model.create()
        .then(function(response){
          console.log(response);
        });
    };
  }

})();
