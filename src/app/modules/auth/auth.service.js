(function () {
  'use strict';

  angular
    .module('gc.auth')
    .service('AuthService', AuthService);

  function AuthService($auth, localStorageService, $http, $state, $rootScope, $q) {
    var self = this,
      prefix = 'gc-auth';

    var storageMap = [
      'id', 'refresh_token', 'token_type', 'expires_in'
    ];

    this.user = {};
    this.afterLoginState = null;

    this.isAuthenticated = function() {
      return (_.isString($auth.getToken()));
    };

    var composeKeyName = function(name) {
      return prefix + '-' + name;
    };

    this.getRefreshToken = function() {
      return localStorageService.get(composeKeyName('refresh_token'));
    };

    var updateUserData = function(response) {
      var data = response.data;
      $auth.setToken(data.access_token);
      self.storeAuthInfo(response);
    };

    this.storeAuthInfo = function (response) {
      _.each(storageMap, function (item) {
        var value = response.data[item],
          storeKeyName = prefix + '-' + item;
        localStorageService.set(storeKeyName, value);
      });
    };

    this.clearAuthInfo = function() {
      _.each(storageMap, function(item){
          var storeKeyName = prefix + '-' + item;
          localStorageService.remove(storeKeyName, item);
      });
      $auth.removeToken();
    };

    this.refreshToken = function(refreshToken) {
      return $http.post('/api/sessions/refresh', {refresh_token: refreshToken})
        .then(function(response){
          updateUserData(response);
          return response;
        });
    };

    this.login = function(username, password) {
      $rootScope.$broadcast('$loginStart');
      var credentials = {username: username, password: password};
      return $auth.login(credentials)
        .then(function (response) {
          $rootScope.$broadcast('$loginSuccess', response);
          self.storeAuthInfo(response);
          if (self.afterLoginState) {
            var tmpState = angular.copy(self.afterLoginState);
            self.afterLoginState = null;
            $state.go(tmpState.name, tmpState.params);
          }
          return response;
        }, function(responseError){
          $rootScope.$broadcast('$loginError', responseError);
          return $q.reject(responseError);
        });
    };

    this.loginRequired = function() {
      if (!self.isAuthenticated()) {
        self.afterLoginState = {name: $state.current.name, params: $state.current.params};
        $state.go('auth.login');
      }
    };

    this.logout = function() {
      $rootScope.$broadcast('$logoutStart');
      return $http.delete('/api/sessions')
        .then(function(response){
          self.clearAuthInfo();
          $rootScope.$broadcast('$logoutSuccess', response);
          $state.go('auth.login');
          return response;
        }, function(responseError) {
          self.clearAuthInfo();
          $state.go('auth.login');
          $rootScope.$broadcast('$logoutError', responseError);
          return $q.reject(responseError);
        });
    }

  }

})();
