(function(){
  'use strict';

  angular
    .module('gc.auth')
    .factory('AuthHttpInterceptor', AuthHttpInterceptor)
    .config(['$httpProvider', function($httpProvider) {
      $httpProvider.interceptors.push('AuthHttpInterceptor');
    }]);

  function AuthHttpInterceptor($q, $injector) {
    return {
      responseError: function(responseError) {
        if (responseError.status === 401) {

          var authService = $injector.get('AuthService'),
            $http = $injector.get('$http'),
            $state = $injector.get('$state'),
            refreshToken = authService.getRefreshToken();

          if (refreshToken && responseError.config.url.match(/\/api\/(.*)/)) {
            return authService.refreshToken(refreshToken)
              .then(function() {
                return $http(responseError.config);
              }, function(err){
                AuthService.clearAuthInfo();
                $state.go('auth.login');
                return $q.reject(err);
              });
          }
        }
        return $q.reject(responseError);
      }
    };
  }
})();
