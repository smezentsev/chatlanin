(function(){
  'use strict';

  angular.module('gc.auth')
    .factory('User', UserModel);

  function UserModel(modelFactory) {
    return function User() {
      var self = this;

      this.meta = ['id', 'email', 'password', 'password_repeat'];
      this.attributes = {};

      if (_.isArray(this.meta)) {
        _.each(this.meta, function(attr) {
          self.attributes[attr] = null;
        })
      }

      var resource = new modelFactory('/api/users', this.meta);
      this.create = function() {
        return resource.provider.create(self.attributes).$promise;
      };
    };
  }

})();
