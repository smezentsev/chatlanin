(function($window){
  'use strict';

  angular
    .module('gc.auth')
    .constant('_', window._)
    .config(AuthConfig);


  function AuthConfig($authProvider, $stateProvider, $urlRouterProvider) {
    $authProvider.loginUrl = '/api/sessions';
    $authProvider.tokenName = 'access_token';

    $urlRouterProvider.otherwise('/login');

    $stateProvider
      .state('auth', {
        url: '',
        parent: 'root',
        abstract: true,
        controller: 'AuthController',
        controllerAs: 'authCtl',
        templateUrl: 'app/modules/auth/templates/main.html',
        data: {
          loginRequired: false
        }
      })
        .state('auth.login', {
          url: '/sign-in',
          controller: 'LoginController',
          controllerAs: 'loginCtl',
          templateUrl: 'app/modules/auth/templates/login.html',
          titleIcon: 'arrow-circle-o-left',
          breadcrumbs: {
            showSingle: false
          },
          topNav: 'auth.login.navbar',
          topNavPosition: '>',
          topNavIcon: 'cogs',
          order: '3'
        })
        .state('auth.signup', {
          url: '/sign-up',
          controller: 'SignupController',
          controllerAs: 'signupCtl',
          templateUrl: 'app/modules/auth/templates/signup.html',
          topNav: 'auth.signup.navbar',
          topNavPosition: '>',
          topNavIcon: 'pencil',
          order: '2'
        })
        .state('auth.reset_password', {
          url: '/reset-password',
          controller: 'ResetPasswordController',
          controllerAs: 'resetPassCtl',
          templateUrl: 'app/modules/auth/templates/reset_password.html'
        });
  }


})();
