(function() {
  'use strict';

  angular
    .module('gc.auth', ['ngCookies', 'ui.router', 'LocalStorageModule', 'satellizer', 'gc.submit', 'gc.form', 'gc.model']);
})();
