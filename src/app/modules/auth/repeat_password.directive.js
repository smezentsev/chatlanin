(function(){
  'use strict';

  angular.module('gc.auth')
    .directive('repeatPassword', repeatPassword);

  function repeatPassword() {
    var directive = {
      restrict: 'A',
      require: ['^form', 'ngModel'],
      link: function(scope, element, attrs, ctrls) {
        var formCtrl = ctrls[0],
          modelCtrl = ctrls[1];

        var mainFieldName = attrs.repeatPassword || 'password',
          mainPasswordFieldCtrl = formCtrl[mainFieldName];

        modelCtrl.$validators['missmatch'] = (function(modelValue, viewValue){
          return viewValue === mainPasswordFieldCtrl.$viewValue;
        });

        mainPasswordFieldCtrl.$viewChangeListeners.push(function() {
          modelCtrl.$validate();
        });
      }
    };

    return directive;
  }

})();
