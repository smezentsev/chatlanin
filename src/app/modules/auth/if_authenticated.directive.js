(function(){
  'use strict';

  angular.module('gc.auth')
    .directive('ifAuthenticated', function(ngIfDirective, AuthService) {
    var ngIf = ngIfDirective[0];

    return {
      transclude: ngIf.transclude,
      priority: ngIf.priority - 1,
      terminal: ngIf.terminal,
      restrict: ngIf.restrict,
      link: function(scope, element, attributes) {
        var ifAuthCheckEval = scope.$eval(attributes.ifAuthCheck),
          ifAuthCheck = _.isBoolean(ifAuthCheckEval) ? ifAuthCheckEval : true;

        var directionEval = scope.$eval(attributes.ifAuthenticated),
          direction = _.isBoolean(directionEval) ? directionEval : true;
        // find the initial ng-if attribute
        var initialNgIf = attributes.ngIf, ifEvaluator;
        // if it exists, evaluates ngIf && ifAuthenticated
        if (initialNgIf) {
          ifEvaluator = function () {
            return scope.$eval(initialNgIf) && ((ifAuthCheck && (direction === AuthService.isAuthenticated())) || !ifAuthCheck);
          }
        } else { // if there's no ng-if, process normally
          ifEvaluator = function () {
            return (ifAuthCheck && (direction === AuthService.isAuthenticated())) || !ifAuthCheck;
          }
        }
        attributes.ngIf = ifEvaluator;
        ngIf.link.apply(ngIf, arguments);
      }
    };
  });




})();
