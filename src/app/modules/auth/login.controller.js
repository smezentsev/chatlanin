(function () {
  'use strict';

  angular
    .module('gc.auth')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController($scope, $state, AuthService) {
    var vm = this;

    vm.user = {
      username: '',
      password: ''
    };

    $scope.processing = false;

    vm.login = function () {
      return AuthService.login(vm.user.username, vm.user.password)
        .then(function(response){
          $state.go('home', {});
          return response;
        })
    };
  }

})();
