(function(){
  'use strict';

  angular.module('gc.bundle', ['gc.auth', 'gc.form', 'gc.submit', 'gc.appConfig', 'gc.breadcrumbs', 'gc.pageTitle', 'gc.faIcon'])

})();
