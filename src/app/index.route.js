(function () {
  'use strict';

  angular
    .module('chatlanin')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider, $translateProvider) {
    $stateProvider
      .state('root', {
        url: '',
        abstract: true,
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('home', {
        url: '/',
        parent: 'root',
        templateUrl: 'app/main/trash.html',
        data: {
          loginRequired: false,
          authCheck: false
        },
        controller: function() {
        },
        controllerAs: 'trash',
        showTitle: false,
        topNav: 'common.home',
        topNavIcon: 'files-o',
        order: '1'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
