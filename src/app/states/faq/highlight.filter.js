(function(){
  'use strict';

  angular.module('faq')
    .filter('highlight', highlight);

  function highlight() {
    return function(text, pattern, wrapFunction) {

      if (!_.isFunction(wrapFunction)) {
        wrapFunction = function(str) {
          return '<b>' + str + '</b>';
        }
      }

      return text.replace(pattern, function(str) {
        return wrapFunction(str);
      });
    }
  }


})();
