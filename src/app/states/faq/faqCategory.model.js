(function(){

  angular.module('faq')
    .service('FaqCategory', faqModel);

  function faqModel(modelFactory, Faq) {
    return function FaqCategory(attributes) {
      var self = this;

      this.attributes = attributes || {};
      var meta = ['id', 'name', 'faqs'];

      this.classMap = {
        faqs: Faq
      };

      this.__proto__ = new modelFactory('/app/mocks/faq-categories.json', meta, self);
      this.constructor = FaqCategory;
    };
  }


})();
