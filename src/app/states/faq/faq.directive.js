(function(){
  'use strict';

  angular.module('faq')
    .directive('faqCategory', faqCategory)
    .directive('faqItem', faqItem);

  function faqCategory() {
    var directive = {
      restrict: 'E',
      scope: {
        models: '=models'
      },
      templateUrl: 'app/states/faq/faq.category.html',
      controller: function($scope) {
        $scope.search = '';
      }
    };

    return directive;
  }

  function faqItem() {
    var directive = {
      restrict: 'E',
      scope: {
        model: '=model'
      },
      require: '^faqCategory',
      templateUrl: 'app/states/faq/faq.item.html',
      link: function(scope, element, attrs, faqCategoryCtrl) {

      },
      controller: function($scope, $filter) {

        var getBody = function() {
          return $scope.model.attributes.body;
        };

        var highlightFn = function(str) {
          return '<span class="faq-highlight">'+str+'</span>'
        };

        $scope.tempBody = $scope.model.attributes.body;
        $scope.opened = false;

        $scope.toggle = function() {
          $scope.opened = !$scope.opened;
        };

        $scope.$on('faq:search', function(e, data) {
          var keyword = data.keyword,
            originBody = getBody(),
            pattern = new RegExp(keyword, 'ig');

          if (originBody.match(pattern)) {
            $scope.opened = true;
            $scope.tempBody = $filter('highlight')(originBody, pattern, highlightFn);
          } else {
            $scope.opened = false;
            $scope.tempBody = originBody;
          }
        });

        $scope.$on('faq:dehighlight', function(){
          $scope.tempBody = getBody();
        });

        $scope.$on('faq:open', function() {
          $scope.opened = true;
        });

        $scope.$on('faq:close', function() {
          $scope.opened = false;
        });
      }
    };

    return directive;
  }


})();
