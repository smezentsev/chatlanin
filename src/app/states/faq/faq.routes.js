(function(){
  'use strict';

  return angular.module('faq')
    .config(config);

  function config($stateProvider) {
    $stateProvider.state('faq', {
      data: {
        loginRequired: false,
        authCheck: false
      },
      parent: 'root',
      url: '/faq',
      topNav: 'common.faq',
      controller: 'FaqController',
      controllerAs: 'faqCtrl',
      templateUrl: 'app/states/faq/faq.html',
      order: '2',
      topNavIcon: 'file-text-o'
    });
  }

})();
