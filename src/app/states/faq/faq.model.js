(function(){

  angular.module('faq')
    .service('Faq', faqModel);

  function faqModel(modelFactory) {
    return function Faq(attributes) {
      var self = this;

      this.attributes = attributes || {};
      var meta = ['id', 'name', 'category_id', 'body'];

      this.__proto__ = new modelFactory('/app/mocks/faq.json', meta, self);
      this.constructor = Faq;
    };
  }


})();
