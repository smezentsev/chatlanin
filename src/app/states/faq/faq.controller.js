(function () {
  'use strict';

  return angular.module('faq')
    .controller('FaqController', FaqController);

  function FaqController($scope, $rootScope, FaqCategory) {

    var vm = this;

    var model = new FaqCategory();

    var init = function() {
      $scope.keyword = '';
      $scope.count = 0;

      model.query().$promise
        .then(function (response) {
          vm.models = response.data;
        });
    };

    init();

    $rootScope.$on('$translateChangeSuccess', function(){
      console.log(11);
      init();
    });

    $scope.search = function () {
      var pattern = new RegExp($scope.keyword, 'ig'),
        tfaqs = [];
      _.each(_.map(vm.models, 'attributes.faqs'), function (faqs) {
        _.each(faqs, function (faq) {
          tfaqs.push(faq);
        })
      });

      var count = 0;
      _.each(_.map(tfaqs, 'attributes.body'), function (body) {
        var matchResult = body.match(pattern);
        if (_.isArray(matchResult))
          count += matchResult.length;
      });

      $scope.count = count;
      $scope.$broadcast('faq:search', {keyword: $scope.keyword});
    };

    $scope.openAll = function () {
      $scope.$broadcast('faq:open');
    };

    $scope.closeAll = function () {
      $scope.$broadcast('faq:close');
    };

    $scope.removeHighlights = function () {
      $scope.$broadcast('faq:dehighlight');
    }
  }

})();
