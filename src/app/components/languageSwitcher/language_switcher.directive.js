(function(){
  'use strict';

  angular.module('chatlanin')
    .directive('languageSwitcher', languageSwitcher);

  function languageSwitcher() {

    var directive = {
      restrict: 'EA',
      link: function(scope, element, attrs) {

        var clickCallback = function(e) {
          element.removeClass('active');
          angular.element('html').unbind('click', clickCallback);
        };

        element.on('click', function(e){
          e.preventDefault();
          e.stopPropagation();
          element.toggleClass('active');
          angular.element('html').bind('click', clickCallback);
        });
      },
      templateUrl: function(element, attrs) {
        return (attrs && attrs.templateUrl ? attrs.templateUrl : 'app/components/languageSwitcher/main.html')
      },
      controller: LanguageSwitcherController,
      controllerAs: 'langSwitcherCtrl'
    };

    return directive;

    /** @ngInject */
    function LanguageSwitcherController($appConfig, $translate) {
      var vm = this;

      vm.currentLanguage = $translate.use();

      vm.languages = $appConfig.config.languages;

      vm.changeLanguage = function(key) {
        $translate.use(key);
        vm.currentLanguage = key;
      };
    }

  }










})();
