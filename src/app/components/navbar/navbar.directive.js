(function() {
  'use strict';

  angular
    .module('chatlanin')
    .directive('navbar', navbar)
    .directive('navbarItem', navbarItem);

  /** @ngInject */
  function navbar(AuthService, $state) {
    var directive = {
      restrict: 'E',
      templateUrl: function(element, attrs) {
        return (attrs && attrs.templateUrl ? attrs.templateUrl : 'app/components/navbar/navbar.html')
      },
      scope: {
        navPosition: '@navPosition'
      },
      link: function(scope, element, attrs) {
        scope.isAuthorized = AuthService.isAuthenticated;
        element.find('.x-navigation-control').click(function(){
          element.find('.x-navigation').toggleClass('x-navigation-open');
        });

        var position = scope.navPosition;

        scope.states = _.sortBy(_.filter($state.get(), function(state) {
          return state[position + 'Nav'] && !state.abstract;
        }), 'order');
      },
      controller: NavbarController,
      controllerAs: 'navbarCtrl'
    };

    return directive;

    /** @ngInject */
    function NavbarController($scope, AuthService, $rootScope) {
      var vm = this;

      $rootScope.$on('$stateChangeSuccess', function(e){
        $scope.$broadcast('state:change:success');
      });

      vm.logout = function() {
        AuthService.logout();
      };

    }
  }

  function navbarItem($state) {

    var resolveActive = function ($state, element, dependState) {
      if ($state.includes(dependState)) {
        element.addClass('active');
      } else {
        element.removeClass('active');
      }
    };

    var directive = {
      restrict: 'A',
      require: '^navbar',
      link: function(scope, element, attrs){
        if (_.isString(attrs.dependState)) {
          resolveActive($state, element, attrs.dependState);
          scope.$on('state:change:success', function() {
            resolveActive($state, element, attrs.dependState);
          })
        }

      }
    };

    return directive;
  }


})();
